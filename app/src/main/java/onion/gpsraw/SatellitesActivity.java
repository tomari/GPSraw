package onion.gpsraw;

import android.content.Context;
import android.content.res.Resources;
import android.location.GnssStatus;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.NumberFormat;

public class SatellitesActivity extends GPSActivity {
	private String checkMark;
	private String uncheckMark;
	private NumberFormat nf;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_satellites);
		addUpToActionbar();
		checkMark=getResources().getString(R.string.checkmark);
		uncheckMark=getResources().getString(R.string.uncheckmark);
		nf=NumberFormat.getInstance();
	}
	private void addUpToActionbar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	@Override
	public void onResume() {
		super.onResume();
		LocationManager lm=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
	}
	@Override
	public void onPause() {
		LocationManager lm=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
		super.onPause();
	}
	@Override
	protected ProgressBar satelliteProgressBar() {
		return (ProgressBar) findViewById(R.id.satProgressBar);
	}
	@Override
	protected TextView numSatellitesTextView() {
		return (TextView) findViewById(R.id.satNumSatelliteTextView);
	}
	protected TextView latitudeTextView() {
		return (TextView) findViewById(R.id.satLatTextView);
	}
	protected TextView longitudeTextView() {
		return (TextView) findViewById(R.id.satLongTextView);
	}
	protected TextView altitudeTextView() {
		return (TextView) findViewById(R.id.satAltTextView);
	}
	protected TextView accuracyTextView() {
		return (TextView) findViewById(R.id.satAccuracyTextView);
	}
	private void updateTableWithSatellites(GnssStatus satellites) {
		TableLayout tab=(TableLayout)findViewById(R.id.prntable);
		int row=1;
		int nSatellites=satellites.getSatelliteCount();
		for(int i=0; i<nSatellites; i++) {
			TableRow r=(TableRow) tab.getChildAt(row++);
			if(r==null) { r=makeTableRow(tab); }
			r.setVisibility(View.VISIBLE);
			((TextView)r.getChildAt(0)).setText(Integer.toString(satellites.getSvid(i)));
			((TextView)r.getChildAt(1)).setText(nf.format(satellites.getAzimuthDegrees(i)));
			((TextView)r.getChildAt(2)).setText(nf.format(satellites.getElevationDegrees(i)));
			((TextView)r.getChildAt(3)).setText(nf.format(satellites.getCn0DbHz(i)));
			((TextView)r.getChildAt(4)).setText(satellites.hasAlmanacData(i)?checkMark:uncheckMark);
			((TextView)r.getChildAt(5)).setText(satellites.hasEphemerisData(i)?checkMark:uncheckMark);
			((TextView)r.getChildAt(6)).setText(satellites.usedInFix(i)?checkMark:uncheckMark);
		}
		for(; row<tab.getChildCount(); row++) {
			tab.getChildAt(row).setVisibility(View.GONE);
		}
	}
	private TableRow makeTableRow(TableLayout tab) {
		TableRow r=new TableRow(this);
		for(int j=1; j<=7; j++) {
			TextView t=makeTextViewForTableRow();
			t.setTextAppearance(android.R.style.TextAppearance);
			t.setText(Integer.toString(j));
			r.addView(t);
		}
		r.setBackgroundColor(getResources().getColor(
				((tab.getChildCount()&0x01)>0)?R.color.table_oddrow:
					R.color.table_evenrow));
		tab.addView(r);
		return r;
	}
	private TextView makeTextViewForTableRow() {
		TextView t=new TextView(this);
		t.setTextIsSelectable(true);
		t.setGravity(Gravity.RIGHT);
		Resources r=getResources();
		t.setPadding(r.getDimensionPixelOffset(R.dimen.padding_table_row), 0, 
				r.getDimensionPixelOffset(R.dimen.padding_table_row), 0);
		return t;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int menuId=item.getItemId();
		if(menuId==android.R.id.home) {
			finish();
		} else {
			return super.onOptionsItemSelected(item);
		}
		return true;
	}
	@Override
	protected void onGnssStatusUpdate(GnssStatus status) {
		super.onGnssStatusUpdate(status);
		updateTableWithSatellites(status);
	}
}
